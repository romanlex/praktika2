package consumers;

/**
 * Created by roman on 10.02.17.
 */
public class Consumer {
    public static volatile int kube;
    public static volatile int kvadrat;
    public static volatile int simple;

    public void message() {
        System.out.println("Cube: " + kube);
        System.out.println("Quad: " + kvadrat);
        System.out.println("Simple: " + simple);
        System.out.println("Result = " + (kube + kvadrat + simple));
    }

}

